<?php

require APPLICATION_PATH . '/Example.php';

class ExampleTest extends PHPUnit_Framework_TestCase {
	
	/**
	* Metoodo test para prueba PHP-UNIT
	*/
	public function testSomething() {
		$example = new Example();
		$this->assertEquals($example->foo(), 'bar');
	}

        /**
        * Metoodo test para prueba PHP-UNIT
        */
        public function testSomethingError() {
                $example = new Example();
                $this->assertEquals($example->foo(), 'barr');
        }
	
}
